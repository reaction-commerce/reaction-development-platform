###############################################################################
### Reaction Platform Configuration
###
### This file defines configuration used in the Makefile.
### You may add and/or override these values with your own custom configuration
### in `config.local.mk`.
###
### Please see GNU Makes multi-line variable documentation for more info.
### https://www.gnu.org/software/make/manual/html_node/Multi_002dLine.html
###############################################################################


# List of tools that must be installed.
# A simple check to determine the tool is available. No version check, etc.
define REQUIRED_SOFTWARE
docker \
docker-compose \
git \
node \
yarn
endef

# Defined here are the subprojects in a comma-separated format
# GIT_REPO_URL,SUBDIR_NAME,TAG
# GIT_REPO_URL is the URL of the git repository
# SUBDIR_NAME is just the directory name itself
# TAG is the git tag or branch to checkout
# Projects will be started in this order
define SUBPROJECT_REPOS
https://gitlab.com/reaction-commerce/reaction.git,reaction,v4.1.5 \
https://gitlab.com/reaction-commerce/reaction-admin.git,reaction-admin,v4.0.0-beta.13 \
https://gitlab.com/reaction-commerce/example-storefront.git,example-storefront,v5.1.1
endef

# These are all the plugins that `make clone-api-plugins` will clone.
# `api-core` is not technically a plugin but is useful to have in the
# same place.
define API_PLUGIN_REPOS
https://gitlab.com/reaction-commerce/api-core.git \
https://gitlab.com/reaction-commerce/api-plugin-accounts.git \
https://gitlab.com/reaction-commerce/api-plugin-address-validation-test.git \
https://gitlab.com/reaction-commerce/api-plugin-address-validation.git \
https://gitlab.com/reaction-commerce/api-plugin-authentication.git \
https://gitlab.com/reaction-commerce/api-plugin-authorization-simple.git \
https://gitlab.com/reaction-commerce/api-plugin-carts.git \
https://gitlab.com/reaction-commerce/api-plugin-catalogs.git \
https://gitlab.com/reaction-commerce/api-plugin-discounts-codes.git \
https://gitlab.com/reaction-commerce/api-plugin-discounts.git \
https://gitlab.com/reaction-commerce/api-plugin-email-smtp.git \
https://gitlab.com/reaction-commerce/api-plugin-email-templates.git \
https://gitlab.com/reaction-commerce/api-plugin-email.git \
https://gitlab.com/reaction-commerce/api-plugin-files.git \
https://gitlab.com/reaction-commerce/api-plugin-i18n.git \
https://gitlab.com/reaction-commerce/api-plugin-inventory-simple.git \
https://gitlab.com/reaction-commerce/api-plugin-inventory.git \
https://gitlab.com/reaction-commerce/api-plugin-job-queue.git \
https://gitlab.com/reaction-commerce/api-plugin-navigation.git \
https://gitlab.com/reaction-commerce/api-plugin-notifications.git \
https://gitlab.com/reaction-commerce/api-plugin-orders.git \
https://gitlab.com/reaction-commerce/api-plugin-payments-example.git \ 
https://gitlab.com/reaction-commerce/api-plugin-payments-stripe.git \
https://gitlab.com/reaction-commerce/api-plugin-payments.git \
https://gitlab.com/reaction-commerce/api-plugin-pricing-simple.git \
https://gitlab.com/reaction-commerce/api-plugin-products.git \
https://gitlab.com/reaction-commerce/api-plugin-settings.git \
https://gitlab.com/reaction-commerce/api-plugin-shipments-flat-rate.git \
https://gitlab.com/reaction-commerce/api-plugin-shipments.git \
https://gitlab.com/reaction-commerce/api-plugin-shops.git \
https://gitlab.com/reaction-commerce/api-plugin-simple-schema.git \
https://gitlab.com/reaction-commerce/api-plugin-sitemap-generator.git \
https://gitlab.com/reaction-commerce/api-plugin-surcharges.git \
https://gitlab.com/reaction-commerce/api-plugin-system-information.git \
https://gitlab.com/reaction-commerce/api-plugin-tags.git \
https://gitlab.com/reaction-commerce/api-plugin-taxes-flat-rate.git \
https://gitlab.com/reaction-commerce/api-plugin-taxes.git \
https://gitlab.com/reaction-commerce/api-plugin-translations.git
endef

# List of user defined networks that should be created.
define DOCKER_NETWORKS
reaction.localhost
endef
